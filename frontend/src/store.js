import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

Vue.use(Vuex);
const vuexLocal = new VuexPersistence({
  storage: window.localStorage
})

export default new Vuex.Store({
  plugins: [vuexLocal.plugin],
  state: {
    cookie_accepted: false
  },
  getters: {
    cookie_accepted: state => {
      return state.cookie_accepted;
    }
  },
  mutations: {
    accept_cookie(state, payload) {
      state.cookie_accepted = payload;
    }
  },
  actions: {
    cookie_accept({commit}, payload = true) {
      commit('accept_cookie', payload);
    }
  }
})
